package edu.example.mynvvmapp.ui.main

class ProfileRepositoryImplementation(
    private val api : ProfileApi
) :ProfileViewModel.Repository{
    override suspend fun loadProfile(): Profile = api.load()

    override suspend fun logout() = api.logout()

}