package edu.example.mynvvmapp.ui.main

class Profile(
    val id: Int,
    val name: String,
    val email: String
)